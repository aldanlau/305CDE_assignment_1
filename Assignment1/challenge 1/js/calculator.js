


function Calculator(){
    
    this.cal=function(num1,num2,oper){
        var res=0;
        switch(oper){
            case "+":
                res=num1+num2;
                break;
            case "-":
                res=num1-num2;
                break;
            case "*":
                res=num1*num2;
                break;
            case "/":
                res=num1/num2;
                break;
        }
        return res;
    }
}

var calculator=new Calculator();


var val=0; //Declared value variables
var xval=0;//Declared a floating point number variables
var temp=0; //Declared first input value variables
var oper="";//Declared operator variables

/*Get input number*/
function NumPressed(e){
    
    val=e.value
    var xsval=document.getElementById("inp1");
    xsval.value+=val;
    //String to floating number
    xval=parseFloat(xsval.value);
    
}

/* Clear calculate result*/
function ClrPressed(e){
    
    var xsval=document.getElementById("inp1");
    if(e.value=="Clear"){
        xsval.value="";
        
    }
}

/*Input operator*/
function OperPressed(e){
    
    oper=e.value;
    if (e.value=="+"){
        var xsval=document.getElementById("inp1");
        //Save the last results, and change to floating number
        temp=parseFloat(xsval.value);
        //The first input value is empty
        xsval.value="";
    }else if(e.value=="-"){
        var xsval=document.getElementById("inp1");
        temp=parseFloat(xsval.value);
        xsval.value="";
    }else if(e.value=="*"){
        var xsval=document.getElementById("inp1");
        temp=parseFloat(xsval.value);
        xsval.value="";
    }else if(e.value=="/"){
        var xsval=document.getElementById("inp1");
        temp=parseFloat(xsval.value);
        xsval.value="";
    }
}

/*calculate result*/
function EquelPressed(e){
    
    var xsval=document.getElementById("inp1");
    if(e.value=="=")
        
        {
       
        xsval.value=calculator.cal(temp,xval,oper);
    
        }
}